package testcases;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription ="Login to leaftaps";
		testNodes = "Leads";
		authors ="Arjun";
		category = "smoke";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void loginLogout(HashMap<String,String> eachRowMap) {
		new LoginPage() 
		.enterUsername(eachRowMap.get("UserName"))
		.enterPassword(eachRowMap.get("Password"))  
		.clickLogin()
		.clickLogout();
	}

}











