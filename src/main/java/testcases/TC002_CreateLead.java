package testcases;

import java.util.HashMap;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription ="Create Lead";
		testNodes = "CreateLeads";
		authors ="Arjun";
		category = "smoke";
		dataSheetName="TC002";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(HashMap<String,String> eachRowMap) {
		new LoginPage() 
		.enterUsername(eachRowMap.get("UserName"))
		.enterPassword(eachRowMap.get("Password"))  
		.clickLogin()
		.clickCRM().clickCreateLead().enterCompName(eachRowMap.get("CompanyName")).enterFirstName(eachRowMap.get("FirstName"))
		.enterLastName(eachRowMap.get("LastName")).enterURL(eachRowMap.get("Url")).clickSubmit()
		.verifyFirstName(eachRowMap.get("FirstName"));
	}

}
