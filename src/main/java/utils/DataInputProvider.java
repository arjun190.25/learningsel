package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataInputProvider{

	public static Object[][] getSheet(String dataSheetName) {		

		Object[][] data = null ;
	
		List<String> headerList = new ArrayList<>();

		try {			
			XSSFWorkbook workbook = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
			XSSFSheet sheet = workbook.getSheetAt(0);	

			// get the number of rows
			int rowCount = sheet.getLastRowNum();
			// get the number of columns
			int columnCount = sheet.getRow(0).getLastCellNum();
			for(int i=0;i<columnCount;i++){
				XSSFRow row = sheet.getRow(0);
				String headerCellValue = row.getCell(i).getStringCellValue();
				headerList.add(headerCellValue);
			}
			System.out.println("Header List is :"+headerList);
			data = new Object[rowCount][1];

			// loop through the rows
			for(int i=1; i <rowCount+1; i++){
				Map<String,String> hp = new HashMap<>();
				try {
					XSSFRow row = sheet.getRow(i);
					for(int j=0; j <columnCount; j++){ // loop through the columns
						try {
							String cellValue = "";
							try{
								cellValue = row.getCell(j).getStringCellValue();
							}catch(NullPointerException e){

							}
							hp.put(headerList.get(j), cellValue);
						} catch (Exception e) {
							e.printStackTrace();
						}				
					}
					System.out.println("Added Map is :"+ hp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				data[i-1][0] = hp;
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Object value is"+data[0][0]+" and "+data[1][0]);
		return data;
	}
}
