package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CRMPage extends ProjectMethods {


	public CRMPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA")WebElement crmSfaLink;
	public MyHomePage clickCRM() {
		click(crmSfaLink);
		return new MyHomePage();
	}
	
}
