package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.How;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {


	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement viewName;
	
	
	public  ViewLeadPage verifyFirstName(String data) {
		verifyExactText(viewName,data);
		return this;
	}
}
