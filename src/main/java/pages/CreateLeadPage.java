package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{


	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement compName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement firstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement lastName;
	@FindBy(how=How.ID, using="createLeadForm_primaryWebUrl") WebElement url;
	@FindBy(how=How.XPATH, using="//input[@value='Create Lead' and @name='submitButton']") WebElement eleSubmit;

	public CreateLeadPage enterCompName(String data) {
		type(compName, data);
		return this;
	}

	public CreateLeadPage enterFirstName(String data) {
		type(firstName, data);
		return this;
	}

	public CreateLeadPage enterLastName(String data) {
		type(lastName, data);
		return this;
	}

	public CreateLeadPage enterURL(String data) {
		type(url, data);
		return this;
	}
	public ViewLeadPage clickSubmit() {
		click(eleSubmit);
		return new ViewLeadPage();

	}
}
